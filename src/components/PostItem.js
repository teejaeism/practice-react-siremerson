import React from "react"

function PostItem({post: {title, body}}){

	
	return(

			<div className="border border-secondary p-3 mb-3">
				<h1>{title}</h1>		
				<p>{body}</p>		
			</div>
		)

}

export default PostItem;