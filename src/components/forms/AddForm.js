import React, {useState} from "react";


function AddForm ({addPost}){

	const [formData, setFormData] = useState({

		title: "",
		body:""

	})

	const onChangeHandler = (e) => {
			setFormData({

					...formData,
					[e.target.name]: e.target.value

			})
	}
	return(

			<form onSubmit={(e)=>addPost(e, formData)}>
			  {JSON.stringify(formData)}
				<input 
				type="text" 
				className="form-control"
				value={formData.title}
				onChange={onChangeHandler}
				name="title"
				/>
				<input 
				type="text" 
				className="form-control"
				value={formData.body}
				onChange={onChangeHandler}
				name="body"
				/>
				<button className="btn btn-success">Submit</button>	
			</form>

		)

}

export default AddForm;