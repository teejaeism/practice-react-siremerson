import React, {useEffect, useState} from "react"
import AddForm from "./components/forms/AddForm";
import PostItem from "./components/PostItem";
import {v4 as uuidv4} from "uuid";


function App(){

	const [showFormStatus, setShowFormStatus] = useState(false)

	const onClickHandle = () => {
		setShowFormStatus(!showFormStatus);
	}



	const [posts, setPosts] = useState([])

	useEffect(()=>{
		fetch("https://jsonplaceholder.typicode.com/posts")
		.then(res => res.json())
		.then(data =>{setPosts(data)})

	},[])

	console.log(posts)

	const allPosts = posts.map(post => (

		<PostItem 
		key={post.id}
		post={post}

		/>

	)).reverse()

	function addPost(e,{title, body}){


		e.preventDefault();
		setPosts([

			...posts,
			{
				userId: 1,
				id: uuidv4(),
				title,
				body
			}
			])
	}

	return(

		<div className="container">
			<button className="btn btn-primary" onClick={onClickHandle}>Add Post</button>
			
			{showFormStatus ? <AddForm addPost={addPost}/> : ""}
			{allPosts}
		</div>
		)


}

//ES6
/*const App = () {
	
		return(

		<div>
		<h1>This is the App Component</h1>
		</div>

		)
}*/



export default App;



